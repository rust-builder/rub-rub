//! Rust Builder for [Rust Builder](https://github.com/rust-builder/rub).
//!
//! This crate is intended for use with
//! [rub](https://github.com/rust-builder/rub).
//!
//! If you don't have `rub` installed, visit https://github.com/rust-builder/rub
//! for installation instructions.
//!
//! # Rub Options
//! <pre>
//! $ rub rub --help
//! "rub - Rust Builder
//!
//! Usage:
//!     rub rub [options] [&lt;lifecycle&gt;...]
//!     rub rub (-h | --help)
//!     rub rub --version
//!
//! Options:
//!     -d --dir &lt;dir&gt;         Set the projects directory.
//!     -b --branch &lt;branch&gt;   Set the build branch. [default: master]
//!     -t --enable-test       Enable tests.
//!     -p --prefix &lt;prefix&gt;   Set the installation prefix. [default: /usr/local]
//!     -u --url &lt;url&gt;         Set the SCM URL.
//!     -h --help              Show this usage.
//!     --update               Update Cargo.toml dependencies.
//!     --target &lt;target&gt;      The compilation target triple.
//!     --features &lt;features&gt;  Features to enable.
//!     --release              Compile with release mode.
//!     --version              Show rust-rub version."
//! </pre>
//!
//! # Examples
//! ```rust
//! # extern crate buildable; extern crate rub_rub; fn main() {
//! use buildable::Buildable;
//! use rub_rub::RubRub;
//!
//! // To run lifecycle methods outside of rub...
//! let mut rr = RubRub::new();
//! let b = Buildable::new(&mut rr, &vec!["rub".to_string(),
//!                                       "rub".to_string(),
//!                                       "--version".to_string()]);
//! assert_eq!(Ok(0), b.version());
//! # }
//! ```
#![experimental]
#![allow(unstable)]
extern crate buildable;
extern crate commandext;
extern crate docopt;
extern crate "rustc-serialize" as rustc_serialize;
extern crate scm;
extern crate utils;

use buildable::{Buildable,BuildConfig,LifeCycle};
use commandext::{CommandExt,to_res};
use scm::git::GitCommand;
use utils::usable_cores;
use utils::empty::to_opt;
use docopt::Docopt;
use std::default::Default;
use std::io::fs;
use std::io::fs::PathExtensions;

static USAGE: &'static str = "rub - Rust Builder

Usage:
    rub <cmd> [options] [<lifecycle>...]
    rub <cmd> (-h | --help)
    rub <cmd> --version

Options:
    -d --dir <dir>         Set the projects directory.
    -b --branch <branch>   Set the build branch. [default: master]
    -t --enable-test       Enable tests.
    -p --prefix <prefix>   Set the installation prefix. [default: /usr/local]
    -u --url <url>         Set the SCM URL.
    -h --help              Show this usage.
    --update               Update Cargo.toml dependencies.
    --target <target>      The compilation target triple.
    --features <features>  Features to enable.
    --release              Compile with release mode.
    --version              Show rub-rub version.";

include!(concat!(env!("OUT_DIR"), "/version.rs"));

#[derive(RustcDecodable)]
struct Args {
    flag_dir: String,
    flag_branch: String,
    flag_enable_test: bool,
    flag_prefix: String,
    flag_url: String,
    flag_help: bool,
    flag_update: bool,
    flag_target: String,
    flag_features: Vec<String>,
    flag_release: bool,
    flag_version: bool,
    arg_lifecycle: Vec<String>,
}

/// rub specific configuration for the `Buildable` lifecycle methods.
#[experimental]
#[derive(Clone,Default)]
pub struct RubRub {
    config: BuildConfig,
    prefix: String,
    url: String,
    update: bool,
    target: String,
    features: Vec<String>,
    release: bool,
}

impl RubRub {
    /// Create a new default RubRub
    pub fn new() -> RubRub {
        Default::default()
    }
}

impl Buildable for RubRub {
    /// Update the RubRub struct after parsing the given args vector.
    ///
    /// Normally, the args vector would be supplied from the command line, but
    /// they can be supplied as in the example below as well.
    ///
    /// # Example
    /// ```rust
    /// # extern crate buildable; extern crate rub_rub; fn main() {
    /// use buildable::Buildable;
    /// use rub_rub::RubRub;
    ///
    /// // To run lifecycle methods outside of rub...
    /// let mut cr = RubRub::new();
    /// let b = Buildable::new(&mut cr, &vec!["rub".to_string(),
    ///                                       "rub".to_string()]);
    /// assert_eq!(Ok(0), b.version());
    /// # }
    fn new(&mut self, args: &Vec<String>) -> &mut RubRub {
        let dargs: Args = Docopt::new(USAGE)
            .and_then(|d| Ok(d.help(false)))
            .and_then(|d| d.argv(args.clone().into_iter()).decode())
            .unwrap_or_else( |e| e.exit());
        self.prefix = dargs.flag_prefix;
        self.url = dargs.flag_url;
        self.update = dargs.flag_update;
        self.target = dargs.flag_target;
        self.features = dargs.flag_features;
        self.release = dargs.flag_release;

        if dargs.flag_version {
            let mut cfg = BuildConfig::new();
            cfg.lifecycle(vec!["version"]);
            self.config = cfg;
        } else if dargs.flag_help {
            let mut cfg = BuildConfig::new();
            cfg.lifecycle(vec!["help"]);
            self.config = cfg;
        } else {
            let mut cfg = BuildConfig::new();

            if to_opt(dargs.flag_dir.as_slice()).is_some() {
                cfg.dir(Path::new(dargs.flag_dir.as_slice()));
            }
            cfg.project("rub");
            if to_opt(dargs.flag_branch.as_slice()).is_some() {
                cfg.branch(dargs.flag_branch.as_slice());
            }
            cfg.test(dargs.flag_enable_test);

            let lc = dargs.arg_lifecycle;
            if to_opt(lc.clone()).is_some() {
                let mut mylc = Vec::new();
                for lc in lc.iter() {
                    mylc.push(lc.as_slice());
                }
                cfg.lifecycle(mylc);
            }
            self.config = cfg;
        }
        self
    }

    /// Get the `BuildConfig` associated with the `RubRub`.
    ///
    /// # Example
    /// ```rust
    /// # extern crate buildable; extern crate rub_rub; fn main() {
    /// use buildable::Buildable;
    /// use rub_rub::RubRub;
    ///
    /// // To run lifecycle methods outside of rub...
    /// let mut cr = RubRub::new();
    /// let b = Buildable::new(&mut cr, &vec!["rub".to_string(),
    ///                                       "rub".to_string()]);
    /// let bc = b.get_bc();
    /// assert_eq!("rub", bc.get_project());
    /// # }
    fn get_bc(&self) -> &BuildConfig {
        &self.config
    }

    /// No lifecycle reorder is necessary for Rust.
    fn reorder<'a>(&self, lc: &'a mut Vec<LifeCycle>) -> &'a mut Vec<LifeCycle> {
        lc
    }

    /// Check for cargo dependencies.
    ///
    /// TODO: Implement
    fn chkdeps(&self) -> Result<u8,u8> {
        Ok(0)
    }

    /// Peform the git operations necessary to get the project directory ready
    /// for the rest of the build lifecycle operations.
    ///
    /// # Notes
    /// * If the project directory doesn't exist, `rub` will be cloned from
    /// github automatically.  You can adjust where is is cloned from by using
    /// the `--url` flag at the command line.
    /// * If the project does exist, the requested branch is updated via
    /// `update_branch` to prepare for the rest of the build cycle.
    fn scm(&self) -> Result<u8,u8> {
        let cfg = &self.config;
        let base = Path::new(cfg.get_dir());
        let u = if self.url.is_empty() {
            "git@github.com:rust-builder/rub-rub.git"
        } else {
            self.url.as_slice()
        };

        let mut res: Result<u8,u8> = if !base.join(cfg.get_project()).exists() {
            GitCommand::new()
                .wd(base.clone())
                .verbose(true)
                .clone(Some(vec![u]), to_res())
        } else {
            Ok(0)
        };

        res = if res.is_ok() {
            GitCommand::new()
                .wd(base.join(cfg.get_project()))
                .verbose(true)
                .update_branch(self.config.get_branch())
        } else {
            res
        };

        res
    }

    /// Run `cargo clean` in the project directory.
    fn clean(&self) -> Result<u8,u8> {
        let cfg = &self.config;
        let wd = Path::new(cfg.get_dir()).join(cfg.get_project());
        let mut cmd = CommandExt::new("cargo");
        cmd.wd(&wd);
        cmd.header(true);
        cmd.args(&["clean"]);
        cmd.exec(to_res())
    }

    /// Run `cargo update` in the project directory if the `--update` flag was
    /// supplied.
    fn configure(&self) -> Result<u8,u8> {
        let cfg = &self.config;
        let wd = Path::new(cfg.get_dir()).join(cfg.get_project());

        fs::unlink(&Path::new(wd.join("dirty"))).unwrap_or_else(|why| {
            println!("{}", why);
        });

        if self.update {
            let mut cmd = CommandExt::new("cargo");
            cmd.wd(&wd);
            cmd.header(true);
            cmd.args(&["update"]);
            cmd.exec(to_res())
        } else {
            Ok(0)
        }
    }

    /// Run 'make' in the project directory.
    ///
    /// Runs `make -j<X>` where X is the number of usable cores.
    fn make(&self) -> Result<u8,u8> {
        let cfg = &self.config;
        let mut jobs = String::from_str("-j");
        jobs.push_str(usable_cores().to_string().as_slice());
        let wd = Path::new(cfg.get_dir()).join(cfg.get_project());
        let mut cmd = CommandExt::new("make");
        cmd.wd(&wd);
        cmd.arg(jobs.as_slice());
        cmd.header(true);
        cmd.exec(to_res())
    }

    /// Run 'make check' in the project directory..
    fn test(&self) -> Result<u8,u8> {
        if self.config.get_test() {
            let cfg = &self.config;
            let wd = Path::new(cfg.get_dir()).join(cfg.get_project());
            let mut cmd = CommandExt::new("make");
            cmd.wd(&wd);
            cmd.args(&["check"]);
            cmd.header(true);
            cmd.exec(to_res())
        } else {
            Ok(0)
        }
    }

    /// Run 'make install' or 'sudo make install' in the project directory.
    ///
    /// Runs `make -j<X> install` where X is the number of usable cores.
    fn install(&self) -> Result<u8,u8> {
        //os_install(&self.config)
        Ok(0)
    }

    /// Not implemented in this crate.
    fn cleanup(&self) -> Result<u8,u8> {
        Ok(0)
    }

    /// Show the docopt USAGE string on stdout.
    fn help(&self) -> Result<u8,u8> {
        println!("{}", USAGE);
        Ok(0)
    }

    /// Show the crate version on stdout.
    fn version(&self) -> Result<u8,u8> {
        println!("{} {} rub-rub {}", now(), sha(), branch());
        Ok(0)
    }
}

#[cfg(test)]
mod test {
    use buildable::{Buildable,BuildConfig};
    use super::RubRub;

    fn check_rr(rr: &RubRub) {
        assert_eq!(rr.prefix, "");
        assert_eq!(rr.url, "");
        assert!(rr.target.is_empty());
        assert!(!rr.update);
        // TODO: complete testing
    }

    fn check_bc(bc: &BuildConfig, lc: &Vec<&str>) {
        let mut tdir = env!("HOME").to_string();
        tdir.push_str("/projects");
        assert_eq!(bc.get_lifecycle(), lc);
        assert_eq!(bc.get_dir().as_str().unwrap(), tdir.as_slice());
        assert_eq!(bc.get_project(), "rub");
        assert_eq!(bc.get_branch(), "master");
        assert!(!bc.get_test());
    }

    #[test]
    fn test_new() {
        let rr = RubRub::new();
        check_rr(&rr);
    }

    #[test]
    fn test_version() {
        let args = vec!["rub".to_string(),
                        "rub".to_string(),
                        "--version".to_string()];
        let mut rr = RubRub::new();
        check_rr(&rr);
        let b = Buildable::new(&mut rr, &args);
        let bc = b.get_bc();
        assert_eq!(bc.get_lifecycle(), &vec!["version"]);
        assert_eq!(b.version(), Ok(0))
    }

    #[test]
    fn test_help() {
        let args = vec!["rub".to_string(),
                        "rub".to_string(),
                        "-h".to_string()];
        let mut rr = RubRub::new();
        check_rr(&rr);
        let b = Buildable::new(&mut rr, &args);
        let bc = b.get_bc();
        assert_eq!(bc.get_lifecycle(), &vec!["help"]);
        assert_eq!(b.help(), Ok(0))
    }

    #[test]
    fn test_base() {
        let args = vec!["rub".to_string(),
                        "rub".to_string()];
        let mut rr = RubRub::new();
        check_rr(&rr);
        let b = Buildable::new(&mut rr, &args);
        let bc = b.get_bc();
        check_bc(bc, &vec!["most"]);
        assert_eq!(b.version(), Ok(0));
    }

    #[test]
    fn test_scm() {
        let args = vec!["rub".to_string(),
                        "rub".to_string(),
                        "scm".to_string()];
        let mut rr = RubRub::new();
        check_rr(&rr);
        let b = Buildable::new(&mut rr, &args);
        let bc = b.get_bc();
        check_bc(bc, &vec!["scm"]);
        assert_eq!(b.version(), Ok(0));
    }

    #[test]
    fn test_all() {
        let args = vec!["rub".to_string(),
                        "rub".to_string(),
                        "all".to_string()];
        let mut rr = RubRub::new();
        check_rr(&rr);
        let b = Buildable::new(&mut rr, &args);
        let bc = b.get_bc();
        check_bc(bc, &vec!["all"]);
        assert_eq!(b.version(), Ok(0));
    }
}
