// build.rs
#![allow(unstable)]
extern crate time;

use std::os;
use std::io::File;
use std::io::process::Command;

fn main() {
    let mut version = "pub fn now() -> &'static str {\n".to_string();

    let mut now = Command::new("date");
    now.arg("--rfc-3339=ns");

    match now.output() {
        Ok(o) => {
            let po = String::from_utf8_lossy(o.output.as_slice());
            version.push_str("    \"");
            version.push_str(po.trim());
            version.push_str("\"\n");
            version.push_str("}\n\n");
        },
        Err(e) => panic!("failed to execute process: {}", e),
    }

    version.push_str("pub fn branch() -> &'static str {\n");

    let mut branch_cmd = Command::new("git");
    branch_cmd.args(&["describe"]);

    match branch_cmd.output() {
        Ok(o) => {
            let po = String::from_utf8_lossy(o.output.as_slice());
            version.push_str("    \"");
            version.push_str(po.trim());
            version.push_str("\"\n");
            version.push_str("}\n\n");
        },
        Err(e) => panic!("failed to execute process: {}", e),
    };

    version.push_str("pub fn sha() -> &'static str {\n");

    let mut sha_cmd = Command::new("git");
    sha_cmd.args(&["rev-parse", "HEAD"]);

    match sha_cmd.output() {
        Ok(o) => {
            let po = String::from_utf8_lossy(o.output.as_slice());
            version.push_str("    \"");
            version.push_str(po.trim());
            version.push_str("\"\n");
            version.push_str("}\n");
        },
        Err(e) => panic!("failed to execute process: {}", e),
    };

    let dst = Path::new(os::getenv("OUT_DIR").unwrap());
    let mut f = File::create(&dst.join("version.rs")).unwrap();
    f.write_str(version.as_slice()).unwrap();
}
